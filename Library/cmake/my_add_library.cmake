# Ensures a given library will be properly installed and exported according to modern CMake conventions.
function(my_add_library LIB_NAME)
	
	# Call regular add_library
	add_library(${ARGV})

	# Add library::library per convention
	add_library(${LIB_NAME}::${LIB_NAME} ALIAS ${LIB_NAME})
	
	# Include header paths
	target_include_directories(${LIB_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/include>
        $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/include>
        $<INSTALL_INTERFACE:include>
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src # Include private headers from /src directory
	)

	# Make sure compiled libs are put into /lib and /bin directories (usually inside build directory)
	set_target_properties(${LIB_NAME} PROPERTIES
		ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib
		LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib
		RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin
	)
	
	# This is needed so that we know where to install to (e.g. set CMAKE_INSTALL_INCLUDEDIR)
	include(GNUInstallDirs)

	# This installs the compiled library.
	# I'm not sure what the INCLUDES DESTINATION ${LIBLEGACY_INCLUDE_DIRS} is for... and what the EXPORT library-targets does exactly.
	install(TARGETS ${LIB_NAME}
			EXPORT ${LIB_NAME}-targets
			LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
			ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
			RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
			INCLUDES DESTINATION ${LIBLEGACY_INCLUDE_DIRS}
	)
	
	# This installs the headers for the library
	install(DIRECTORY ${CMAKE_SOURCE_DIR}/include/${LIB_NAME}
			DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
	)

	# Installs the library-targets.cmake file
	install(EXPORT ${LIB_NAME}-targets
		FILE ${LIB_NAME}-targets.cmake
		NAMESPACE ${LIB_NAME}::
		DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${LIB_NAME}
	)

	# Helpers functions for creating config files that can be included by other projects to find and use a package.
	include(CMakePackageConfigHelpers)

	# Create a config file for the library. Used by find_package. Config.cmake.in is writting by the user.
	# It helps making the resulting package relocatable by avoiding hardcoded paths in the installed Config.cmake file.
	configure_package_config_file(
		${CMAKE_SOURCE_DIR}/cmake/${LIB_NAME}-config.cmake.in			# Input
		${CMAKE_BINARY_DIR}/cmake/${LIB_NAME}-config.cmake				# Output
		INSTALL_DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${LIB_NAME}	# INSTALL_DESTINATION <path>
	)

	# Create a version file for the library. Used by find_package as well (?).
	write_basic_package_version_file(
		${CMAKE_BINARY_DIR}/cmake/${LIB_NAME}-config-version.cmake
		# VERSION ${PROJECT_VERSION}	 --- PROJECT_VERSION is used by default as VERSUIN (must be set)
		COMPATIBILITY AnyNewerVersion
	)

	# Install the config file for the library so that find_package(library) can work (?)
	install(
		FILES
			${CMAKE_BINARY_DIR}/cmake/${LIB_NAME}-config.cmake
			${CMAKE_BINARY_DIR}/cmake/${LIB_NAME}-config-version.cmake
		DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${LIB_NAME}
	)

	# Make an export set. This puts the targets you have listed into a file in the build directory, and (optionally) prefixes them with a namespace.
	export(EXPORT ${LIB_NAME}-targets
		FILE ${CMAKE_BINARY_DIR}/cmake/${LIB_NAME}-targets.cmake
		NAMESPACE ${LIB_NAME}::
	)
endfunction()

# Make sure you add @PACKAGE_INIT@ at the top of your library-config.cmake before calling this function.

function(my_config_library LIB_NAME)
	set(LIB_CMAKE_DIR ${LIB_NAME}_CMAKE_DIR)
	get_filename_component(${LIB_CMAKE_DIR} "${CMAKE_CURRENT_LIST_FILE}" PATH)
	include(CMakeFindDependencyMacro)

	list(APPEND CMAKE_MODULE_PATH ${LIB_CMAKE_DIR})

	if(NOT TARGET ${LIB_NAME}::${LIB_NAME})
		include("${LIB_CMAKE_DIR}/library-targets.cmake")
	endif()

	set(${LIB_NAME}_LIBRARIES ${LIB_NAME}::${LIB_NAME})
endfunction()