include(${CMAKE_SOURCE_DIR}/cmake/my_add_library.cmake)

# Required to add a library as CMake target
my_add_library(library library.cpp)

# This adds appropriate compiler switches to enable C++17 features.
target_compile_features(library PRIVATE cxx_std_17)
