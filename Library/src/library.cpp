#include <library/library.hpp>

#include <iostream>

namespace library {
	
void say_hi() {
    std::cout << "Hello, World!\n";
}

} // namespace library