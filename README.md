# CMake Boilerplate

Minimal CMake project structures for various purposes (e.g. library/application).

# Library

I have compiled the needed boilerplate code to create a properly exported and installable library.

The code is inside a function called *my_add_library*. Instead of calling add_library(MyLib ...) and worrying about all the other stuff, you will simply include my_add_library.cmake and call it instead.

Code for CMakeLists.txt inside source /src directory:
```
include(${CMAKE_SOURCE_DIR}/cmake/my_add_library.cmake)
my_add_library(MyLib mySource_1.cpp mySource_2.cpp)
# ...
```

